document.addEventListener("DOMContentLoaded", function(event) { 
    homeModule;
    app.loadModule({
        moduleName: 'homeModule',
        filePath: 'home.html',
        loadedFunction: function(){ return homeModule.init()}
    });
});

/***   init HOME page ****/
function init(){
    var newsletterBtn = document.getElementById('submitNewsletter');

    if (newsletterBtn.addEventListener) {     // For all major browsers, except IE 8 and earlier
        newsletterBtn.addEventListener("click", function(){
            validateForm('newsletter');
        }, false);
    } else if (newsletterBtn.attachEvent) {   // For IE 8 and earlier versions
        newsletterBtn.attachEvent("onclick", function(){
            validateForm('newsletter');
        }, false);
    }
}

