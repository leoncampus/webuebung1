var PREVENT_HISTORY_BACK = false;

window.addEventListener('popstate', function popstate(e) {
    if(PREVENT_HISTORY_BACK === false) {
       app.history.handleHistoryBack(e);
    } else {
       
    }
 });

//load needed click events for main html
window.onload = function() {
    loadClickEventsMainPage();
}

//default close modal if click somewhere
window.onclick = function(event) {
    var modal = document.getElementById('loginModal');

    if (event.target == modal) {
        modal.style.display = "none";
    }
}

//default window function when scrolling
window.onscroll = function() { scrollFunction() };

/* CORE App */
var app = ( function app() {
    "use strict";

    /* CORE CONFIGURATION * */
    let config = function config(){
        let conf = {
            mainElement: 'app',  //element to load html into
            path: 'src/html/'
        },

        getHTMLPath = function getHTMLPath(){
            return conf.path;
        },

        getMainElement = function getMainElement(){
            return conf.mainElement;
        };

        return{
            getHTMLPath: getHTMLPath,
            getMainElement: getMainElement
        }
    }(),

    // module that handles browser history for single page application
    history = function history() {
        // LIFO-stack (last in - first out)
        let _history = [],
            _savedInputState = {},
            _handleHistoryBack = true,
            _actualHTML = '',        // stores the actual html name

    // Example implementation that handles the history back button
    handleHistoryBack = function handleHistoryBack() {
       if(_handleHistoryBack === false) {
          return;
       }
       
       PREVENT_HISTORY_BACK = true; // Prevent double history back through "popstate"-Event
       
       _savedInputState = {};
       
       if(length() > 0) {
          var u_state = pop();
          u_state = u_state.history;

          if(u_state.first === false) {
            setActualHTML(u_state.fragment);
            
            _loadHistoryHTML(u_state.fragment);
                let u_lastHistoryItem = getLastEntry();

                if(u_lastHistoryItem.history !== undefined && u_lastHistoryItem.history.savedHistoryBackHandler !== undefined) {                     
                   u_lastHistoryItem.history.savedHistoryBackHandler();
                } else if(u_state.savedHistoryBackHandler !== undefined) {
                   u_state.savedHistoryBackHandler();
                }

          } else {
             add(u_state);
          }
       }
       
       PREVENT_HISTORY_BACK = false;
    },
    
    _loadHistoryHTML = function _loadHistoryHTML(moduleName){
        let filePath = 'home.html',
            successFunction = function(){ return homeModule.init()};

        if(moduleName === 'tableModule'){
            filePath = 'tableModule.html';
            successFunction = function(){ return tableModule.init()};
        }else if(moduleName === 'videosModule'){
            filePath = 'video.html';
            successFunction = function(){ return videoModule.init()};
        }else if(moduleName === 'linksModule'){
            filePath = 'links.html';
            successFunction = function(){ return linksModule.init()};
        }else if(moduleName === 'aboutModule'){
            filePath = 'about.html';
            successFunction = function(){ return aboutModule.init()};
        }

        loadModule({
            moduleName: moduleName,
            filePath: filePath,
            history: 'none',
            loadedFunction: successFunction
        });
    },
    // Adds a new JSON-object to the history stack
    add = function add(object) {
       var u_pageContent = config.getMainElement();
       
       _savedInputState  = {};
       /*
       // Save input state to HTML
       $(u_pageContent).find('input').each(function setValueAndCheckedAttribute() {
          $(this).attr('value', this.value);
       });
       $(u_pageContent).find('textarea').each(function setTextareaContent() {
          $(this).text(this.value);
       });
       $(u_pageContent).find('select').each(function setSelectedOption() {
          $(this).find('option[value="'+this.value+'"]').attr('selected','selected');
          $(this).find('option[value!="'+this.value+'"]').removeAttr('selected');
       });
       */
       // Only change the object if it is not pushed again from handleHistoryBack
       if(object.first === undefined) {
          object.savedHistoryBackHandler = object.historyBackHandler;

          if(getActualHTML().length > 0) {
             object.fragment = getActualHTML();
             object.first    = false;
             
             if(getLastEntry().history !== undefined) {
                object.historyBackHandler = getLastEntry().history.savedHistoryBackHandler;
             }
          } else {
             object.first = true;
          }

          object.html = u_pageContent.innerHTML;
       }
       
       _history.push(object);
       window.history.pushState({nr: _history.length}, null, document.location.href);
    },

    // Retrives the last history item
    pop = function pop() {     
       var u_historyElement = {};

       if(_history.length > 0) {
          u_historyElement = _history.pop();
       }

       window.history.replaceState({nr: _history.length}, null, document.location.href);
       
       return {history: u_historyElement};
    },

    // Deletes an entry of the history
    del = function del(nr) {
       var u_history = $.grep(_history, function(item, idx) {
         return ( idx !== nr );
       });
       
       _history = u_history;
       global.history.replaceState({nr: _history.length}, null, document.location.href);
    },
    
    // Returns the whole history stack
    get = function get() {
       return {history: _history};
    },

    // retrives the last history element without changing the history
    getLastEntry = function getLastEntry() {
       return {history: _history[_history.length]};
    },

    // Returns the length of the history
    length = function length() {
       return _history.length;
    },

    // Resets the history
    clear = function clear() {
       _history = [];
       global.history.replaceState({nr: _history.length}, null, document.location.href);
    },

    setActualHTML = function setActualHTML(html) {
       _actualHTML = html;
    },

    getActualHTML = function getActualHTML() {
       return _actualHTML;
    },

    disable = function disable() {
       PREVENT_HISTORY_BACK = true;
    },

    enable = function enable() {
       PREVENT_HISTORY_BACK = false;
    };
    
    return {
       handleHistoryBack: handleHistoryBack,
       add: add,
       pop: pop,
       get: get,
       getLastEntry: getLastEntry,
       del: del,
       clear: clear,
       length: length,
       setActualHTML: setActualHTML,
       getActualHTML: getActualHTML,
       disable: disable,
       enable: enable
    };
    }(),

    helper = function helper() {
        let 

        extendObject = function extendObject(defaults, options){
            let extended = {},
                prop;

            for (prop in defaults) {
                if (Object.prototype.hasOwnProperty.call(defaults, prop)) {
                    extended[prop] = defaults[prop];
                }
            }

            for (prop in options) {
                if (Object.prototype.hasOwnProperty.call(options, prop)) {
                    extended[prop] = options[prop];
                }
            }

            return extended;
        };

        return {
            extendObject: extendObject
        }
    }(),

    /* LOAD EXTERNAL HTML FILE CONTENT */
    _loadHTMLFile = function _loadHTMLFile(filePath){
        return new Promise(function manipulatePromise(resolve, reject) {  
            filePath = config.getHTMLPath() + filePath;

            const init = {
                method : "GET",
                headers : { "Content-Type" : "text/html" },
                mode : "cors",
                cache : "default"
            };
    
            const req = new Request(filePath, init);
    
            fetch(req).then(function(response) {
                            return response.text()
                        }).then(function(body) {
                            resolve(body);
                        })
        });      
    },

    /* Load needed module HTML and Javascript*/
    loadModule = function loadModule(options) {
        let conf = {
            moduleName: 'homeModule',
            filePath: '',
            history: 'add',
            historyBackHandler : function historyBackHandler() { return true; },
            loadedFunction: function(){}
        }
        
        conf = helper.extendObject(conf, options);

        if(conf.moduleName !== 'homeModule'){
            document.querySelector('.header-banner').style.display = 'none';
            document.querySelector('.topnav').classList.add('nav-dark');
        }else{
            document.querySelector('.header-banner').style.display = 'block';
            document.querySelector('.topnav').classList.remove('nav-dark');
        }

        if(conf.history === 'replace') {
            history.pop(); // Just remove the actual screen
         } else  if(conf.history === 'add') {
            history.add({fragment: conf.moduleName, 
                        historyBackHandler: conf.historyBackHandler
            });
        }

        history.setActualHTML(conf.moduleName);
        

        _loadHTMLFile(conf.filePath).then(function(body){
            document.getElementById(config.getMainElement()).innerHTML = body;
            conf.loadedFunction();
        });
    };

    return { config: config,
             loadModule: loadModule,
             helper: helper,
             history: history
            }
}());

 
/***  WIndow Function ***** */
function loadClickEventsMainPage(){
    var toggleMenuBtn = document.getElementById('menu-icon');

    //before adding click event check browser compatibility
    if (toggleMenuBtn.addEventListener) {     // For all major browsers, except IE 8 and earlier
        toggleMenuBtn.addEventListener("click", showNavMenu);
    } else if (toggleMenuBtn.attachEvent) {   // For IE 8 and earlier versions
        toggleMenuBtn.attachEvent("onclick", showNavMenu);
    }

    var signupBtn = document.getElementById('signup');

    if (signupBtn.addEventListener) {     // For all major browsers, except IE 8 and earlier
        signupBtn.addEventListener("click", showLoginModal);
    } else if (signupBtn.attachEvent) {   // For IE 8 and earlier versions
        signupBtn.attachEvent("onclick", showLoginModal);
    }

    initNavbarEvents();
}

function initNavbarEvents(){
    var homeModuleBtn = document.getElementById('home');

    homeModuleBtn.addEventListener("click", function(event){
        toggleActive(event.target);
        app.loadModule({
            moduleName: 'homeModule',
            filePath: 'home.html',
            loadedFunction: function(){ return homeModule.init()}
        });
    }, false);

    var tableModuleBtn = document.getElementById('table');

    tableModuleBtn.addEventListener("click", function(event){
        toggleActive(event.target);
        app.loadModule({
            moduleName: 'tableModule',
            filePath: 'tableModule.html',
            loadedFunction: function(){ return tableModule.init()}
        });
    }, false);

    var externalLinksModule = document.getElementById('externalLinks');

    externalLinksModule.addEventListener("click", function(event){
        toggleActive(event.target);
        app.loadModule({
            moduleName: 'linksModule',
            filePath: 'links.html',
            loadedFunction: function(){ return linksModule.init()}
        });
    }, false);

    var videoModule = document.getElementById('videos');

    videoModule.addEventListener("click", function(event){
        toggleActive(event.target);
        app.loadModule({
            moduleName: 'videosModule',
            filePath: 'video.html',
            loadedFunction: function(){ return videosModule.init()}
        });
    }, false);

    var videoFooter = document.getElementById('videosFooter');

    videoFooter.addEventListener("click", function(event){
        toggleActive(videoModule);
        app.loadModule({
            moduleName: 'videosModule',
            filePath: 'video.html',
            loadedFunction: function(){ return videosModule.init()}
        });
    }, false);

    var aboutBtn = document.getElementById('about');

    aboutBtn.addEventListener("click", function(event){
        toggleActive(event.target);
        app.loadModule({
            moduleName: 'aboutModule',
            filePath: 'about.html',
            loadedFunction: function(){ return aboutModule.init()}
        });
    }, false);

    var aboutFooter = document.getElementById('aboutFooter');
    aboutFooter.addEventListener("click", function(event){
        toggleActive(aboutBtn);
        app.loadModule({
            moduleName: 'aboutModule',
            filePath: 'about.html',
            loadedFunction: function(){ return aboutModule.init()}
        });
    }, false);
}

function toggleActive(element){
    document.querySelector('.sidebar').querySelector('.active').classList.remove('active')

    element.classList.add('active');
}

function showLoginModal(event){
    document.getElementById('loginModal').style.display = 'block';
    document.getElementById('signupLink').addEventListener('click', toggleSignupForm);

    document.getElementById('loginSubmit').addEventListener('click', function(){
        validateForm('login');
    }, false);
    document.getElementById('registerSubmit').addEventListener('click', 'click', function(){
        validateForm('register');
    }, false);
}

function validateForm(type){
    if(type === 'register'){
        var email = document.querySelector('input[name="email"]').value;
        var username = document.querySelector('input[name="username"]').value;
        var pwd = document.querySelector('input[name="pwd"]').value;

        if(validateInputField('username', username, document.querySelector('input[name="username"]'))){
            if(validateInputField('email', email, document.querySelector('input[name="email"]'))){
                if(validateInputField('pwd', pwd, document.querySelector('input[name="pwd"]'))){
                    //do register
                    showSnackbar('Registration successful!', 'success');
                }
            }
        }
    }else if(type === 'login'){
        var username = document.querySelector('input[name="loginUsername"]').value;
        var pwd = document.querySelector('input[name="loginPwd"]').value;

        if(validateInputField('loginUsername', username, document.querySelector('input[name="loginUsername"]'))){
            if(validateInputField('loginPwd', pwd, document.querySelector('input[name="loginPwd"]'))){
                //do login
                showSnackbar('Login successful!', 'success');
            }
        }
    }else if(type === 'newsletter'){
        var email = document.querySelector('#newsletterForm input[name="email"]').value;

        if(validateInputField('email', email, document.querySelector('#newsletterForm input[name="email"]'))){
            showSnackbar('Successfully subscribed', 'success');
        }
    }
}

function validateInputField(type, value, element){
    //validate if value is not empty
    if(value !== undefined && value.length > 0){
        var regex = '';

        if(type === 'email'){
            regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        }else if(type === 'username' || type === 'loginUsername'){
            regex = /^.{3,}$/;
        }else{
            regex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)[a-zA-Z\d]{8,}$/g;
        }

        if(regex.test(String(value)) === true){
            return true;
        }else{
            showSnackbar('Wrong ' + type + ' format!', 'danger');
            markInvalid(element);
            return false;
        }

    }else{
        showSnackbar('Wrong ' + type + ' format!', 'danger');
        markInvalid(element);
    }
}

//marks an input field as invalid
function markInvalid(element){
    removeInvalidState();
    element.classList.add('invalid');
    setTimeout(function(){
        removeInvalidState();
    }, 3000);
}

function removeInvalidState(){
    //remove invalid state from all input fields
    document.querySelectorAll('input').forEach(function(element){
        element.classList.remove('invalid');
    })
}

function hideSnackbars(){
    var alerts = document.querySelectorAll('.alert-box');   //get all alert boxes, there could be more

    alerts.forEach(function(element){
        element.parentNode.removeChild(element);
    })
}

//shows snackbar message with specific type and text to user
function showSnackbar(text, type){
    var alertBox = document.createElement('div');

    alertBox.classList.add('alert-box');
    alertBox.classList.add(type);

    var closeBtn = document.createElement('span');
    closeBtn.classList.add('closeBtn');
    closeBtn.innerHTML = "x";
    closeBtn.addEventListener("click", hideSnackbars);

    alertBox.appendChild(closeBtn);

    var text = document.createTextNode(text);
    alertBox.appendChild(text);

    document.body.appendChild(alertBox);

    setTimeout(function(){ //show message for 3 seconds
        var element = document.querySelector('.alert-box');
        if(element !== null && element !== undefined){ //alert box could be closed before
            hideSnackbars();
        }
    }, 3000)
}

function toggleSignupForm(event){
    if(document.getElementById('loginForm').style.display === 'none'){
        document.getElementById('loginForm').style.display = 'block';
        document.getElementById('signupForm').style.display = 'none';
        event.target.text = 'Sign up';
    }else{
        document.getElementById('signupForm').style.display = 'block';
        document.getElementById('loginForm').style.display = 'none';
        event.target.text = 'Back to login';
    }
}

function showNavMenu(event){
    let target = document.querySelector(".sidebar");
    let element = event.target;
    if(!element.classList.contains('nav-icon')){//check if inner div is clicked
        element = document.querySelector('.nav-icon');
    }
    if (element.classList.contains('change')) {
        element.classList.remove("change");
        target.style.width = "0px";
    }else{
        element.classList.add("change");
        target.style.width = "20%";
    }
}

function scrollFunction() {
  if (document.body.scrollTop > 80 || document.documentElement.scrollTop > 80) {
    document.querySelector(".topnav").classList.add('fixed');
  } else {
    document.querySelector(".topnav").classList.remove('fixed');
  }
}


/*********  MODULES     *********** */
/* LOGIN Module */
var loginModule = ( function loginModule(){
    
});

/* HOME Module - Newsletter */
var homeModule = ( function homeModule(){
    let 
    
    init = function init(){
        var newsletterBtn = document.getElementById('submitNewsletter');

        if (newsletterBtn.addEventListener) {     // For all major browsers, except IE 8 and earlier
            newsletterBtn.addEventListener("click", function(){
                validateForm('newsletter');
            }, false);
        } else if (newsletterBtn.attachEvent) {   // For IE 8 and earlier versions
            newsletterBtn.attachEvent("onclick", function(){
                validateForm('newsletter');
            }, false);
        }
    };
    
    return {
        init: init
    }
}());

var linksModule = ( function linksModule(){
    let 

    init = function init(){
        //load specific javascript
    };
    
    return {
        init: init
    }
}());

var tableModule = ( function tableModule(){
    let 

    init = function init(){
        //load specific javascript
    };
    
    return {
        init: init
    }
}());

var videosModule = ( function videosModule(){
    let 

    init = function init(){
        //load specific javascript
    };
    
    return {
        init: init
    }
}());

var aboutModule = ( function videosModule(){
    let 

    init = function init(){
        //load specific javascript
    };
    
    return {
        init: init
    }
}());