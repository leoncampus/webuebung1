
#### Run Application

Please use a simple Webserver to run the application - otherwise Browser will block cross origin resource sharing (needed for single page app files)

#### App Details
This app is a single page application, meaning all the page modules are loaded into the element with id "app".

Structure:
- index.html
- src
    - assets
        - image sources
    - css
        - global stylesheet and font awesome icon css
    - html
        - html contents of pages
    - js
        - index.js for initialization of application
        - main.js core of application


Main.js - Core

The main.js file contains the core app. It consist of global configuration (spa element id, html paths), a history module (needed for browser history handling) and modules to load different html files and their javascript modules when needed.
Furthermore the main.js file contains the needed js for page interactivities (open modals, scrolling, form validations, snackbars) as well as the javascript for the modules of the pages.

#### Simple Newsletter Subscription with Input Validation 
The newsletter form can be found on the homepage.
It consists of an input field (email). The input field is either validated with HTML5 pattern/attribute validation (button Login html5 validation) or pure Javascript.

If a wrong format/input value (email format) is given a snackbar is displayed to the user containing an error message with specific text, or a success message if format is correct.


#### Simple Login with Input Validation 

The Login/Sign up module can be found in the navbar of the application ("Sign Up"). If clicked, a modal is opened - you can choose to either login with credentials or sign up.
The Login consists of 2 input fields (username, password).
The Sign up feature consists of 3 input fields (username, email and password). 
The input fields are either validated with HTML5 pattern/attribute validation (button Login html5 validation) or pure Javascript.

If a wrong format/input value is given a snackbar is displayed to the user containing an error message with specific text, also the input field is highlighted.

Validations:
    Username
        must be at least 3 characters long
    E-Mail
        must consist of an @ symbol following a domain
    Password
        must be at least 8 characters long and consist of at least 1 uppercase letter, 1 lowercase letter and a number
